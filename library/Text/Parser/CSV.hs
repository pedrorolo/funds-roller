module Text.Parser.CSV(
    Parser, 
    ParseError, 
    eol, 
    cell,
    customCell, 
    parse, 
    intCell, 
    keyValueLine, 
    emptyCell,
    cellSep) where
        
import BasicPrelude hiding ((<|>),try)
import Text.Parsec
import Text.Parsec.Error 
import Data.Text as Text hiding (null)
import Control.Monad.Identity

type Parser resultType = ParsecT String () Identity resultType



cell :: Parser String
cell = quotedCell <|> noneOf ",\n\r" `manyTill` endOfCell
  where endOfCell = string "," <|> try eol


customCell :: String -> Parser res  -> Parser res
customCell typeName subparser = 
  cell
    >>= either errorHandler 
               return . parse (subparser <* eof) ""
  where 
    errorHandler e = unexpected $ typeName ++ foldMap  ((" " ++) . (++ " ") . messageString) (errorMessages e)

emptyCell :: Parser ()        
emptyCell = 
    cell >>= guard . null 

int :: Parser Int
int = ((read :: Text -> Int) . pack ) <$> many1 digit

intCell :: Parser Int
intCell = customCell "integer" int

-- just an example
keyValueLine :: Parser (String, Int)
keyValueLine = do 
    key <- cell
    char ','
    value <- intCell
    return (key, value)

parseKeyValueColumns :: Either ParseError [(String, Int)]
parseKeyValueColumns = 
    parse keyValueLines "" "dsad,2\ndsa,43"
    where keyValueLines = sepBy keyValueLine eol <* eof

cellSep :: Parser Char
cellSep = char ','

line :: Parser [String]
line = sepBy cell cellSep

file :: Parser [[String]]
file = sepBy line eol <* eof


quotedCell :: Parser String
quotedCell = 
    do char '"'
       content <- many quotedChar
       char '"' <?> "quote at end of cell"
       return content

quotedChar :: Parser Char
quotedChar =
    noneOf "\""
    <|> try (string "\"\"" >> return '"')
  
eol :: Parser String
eol =   try (string "\n\r")
    <|> try (string "\r\n")
    <|> string "\n"
    <|> string "\r"
    <|> (eof >> return "")
    <?> "end of line"


