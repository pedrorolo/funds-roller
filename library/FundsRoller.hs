-- | An FundsRoller module.
module FundsRoller (
  module FundsRoller.Data,
  module BasicPrelude,
  FundsRoller.TransactionsFile.loadFile,
  main
) where
import BasicPrelude
import FundsRoller.Data
import FundsRoller.TransactionsFile

-- | An example function.
main :: IO ()
main = return ()
