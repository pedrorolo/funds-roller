-- | An FundsRoller module.
module FundsRoller.TransactionsFile(loadFile, ParseError) where
import BasicPrelude
import Text.Parsec.String(parseFromFile)
import Text.Parsec(ParseError) 
import FundsRoller.Data(FundBlock)
import FundsRoller.TransactionsFile.Parsers(fundsParser)


loadFile :: String -> IO (Either ParseError [FundBlock])
loadFile =
  parseFromFile fundsParser

