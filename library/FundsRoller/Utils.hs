module FundsRoller.Utils((<$$>)) where

import BasicPrelude
  
(<$$>) :: (Functor f, Functor g) => (a -> b) -> f (g a) -> f (g b)
(<$$>) = fmap . fmap

