module FundsRoller.TransactionsFile.Parsers where
import BasicPrelude hiding (try,(<|>))
import Control.Monad.Identity
import FundsRoller.Data
import Text.Parsec as Parsec
import Text.Parser.CSV
import Data.Text

data TransactionType = Purchase | Sale
data TransactionTypeConfig = TransactionTypeConfig {
  blockHeaderTitleStr :: String,
  transactionTypeStr :: String
}

headersConfig :: TransactionType -> TransactionTypeConfig
headersConfig t = 
  case t of
    Purchase -> 
      TransactionTypeConfig 
        "Entradas"
        "Subscrição de U.P. s"
    Sale ->
      TransactionTypeConfig
        "Saídas"
        "Resgate de UP"



fundsParser :: Parser [FundBlock]
fundsParser = 
  fundBlockParser `sepBy` separator <* eof
  where 
    separator = try $ optional emptyLine >> eol 
    emptyLine = sepBy emptyCell cellSep

fundBlockParser :: Parser FundBlock
fundBlockParser = do
  f <- fundBlockHeader
  string ",,,,Valores da Transação,,,,,,,,Valores por Quota,,,,,,,"
  eol
  purchases <- try purchasesBlock <|> return []
  sales <- try salesBlock <|> return []
  return $ FundBlock f (purchases ++ sales) 

transactionsBlock :: TransactionType -> Parser [Transaction]
transactionsBlock t = 
  header >> (many . try $ transactionWithType t)
  where 
    headerTitle = blockHeaderTitleStr $ headersConfig t
    header = 
        string headerTitle
        >> string ",,,,Valores na moeda original,,,,,,,,Valores na moeda original,,,,,,Valores em Euros,"
        >> eol
        >> string "Data Mov.,,,Tipo de Operação,Quantidade Total,,Preço,Valor Total,,,,,Encargos,,Valor,Câmbio,,,Encargos,Valor"
        >> eol

transactionWithType ::
  TransactionType -> ParsecT String () Identity Transaction
transactionWithType t = do
  Parsec.count 3 cell
  typeCell 
  Parsec.count 16 cell
  return $ Transaction 1 1 
  -- 26-11-2014,,,Resgate de UP,1,,151.13,151.13,,,,,0,,151.13,1,,,0,151.13
  where 
    typeStr = transactionTypeStr $ headersConfig t
    typeCell = customCell "transaction type" $ string typeStr
  
  

purchasesBlock :: Parser [Transaction]
purchasesBlock = transactionsBlock Purchase
    
salesBlock :: Parser [Transaction]
salesBlock = transactionsBlock Sale


fundBlockHeader :: Parser Fund
fundBlockHeader = do 
    (name, isin) <- lineWithFirstCell "name and isin" nameAndISINParser
    country <- lineWithFirstCell "country" $ string "País da Entidade Emitente: " >> many1 anyChar <* eof
    nif <- lineWithFirstCell "nif" $ string "NIF da Entidade Emitente: " >> many1 digit <* eof
    return $ fundFromStrings name isin country nif
  where
    fundFromStrings :: String -> String -> String -> String -> Fund
    fundFromStrings a b c d = Fund (pack a) (pack b) (pack c) (pack d) 



lineWithFirstCell :: String -> Parser res -> Parser res
lineWithFirstCell n p = 
  customCell n p <* emptyCell `manyTill` eol


    
nameAndISINParser ::
  ParsecT String u Identity (String,String)
nameAndISINParser = do  
  name <- anyChar `manyTill` try(string " - ")
  isin <-  many1 anyChar
  eof
  return (name, isin)