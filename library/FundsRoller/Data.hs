module FundsRoller.Data(
  Fund(..),
  Transaction(..),
  FundBlock(..)) where

import BasicPrelude

data Fund = Fund { name :: Text , isin :: Text, country :: Text, nif :: Text} deriving (Show, Eq)

data FundBlock = FundBlock { fund :: Fund, transactions :: [Transaction] } deriving (Show)

data Transaction = Transaction{ price :: Integer, cost :: Integer } deriving (Show)
