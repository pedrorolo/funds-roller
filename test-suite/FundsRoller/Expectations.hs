module FundsRoller.Expectations(rightShouldSatisfy) where
import BasicPrelude
import Test.Hspec

rightShouldSatisfy :: (Show a) => (a -> Bool) -> Either b a -> Expectation
rightShouldSatisfy pred =
    either (const (False `shouldBe` True))
           (`shouldSatisfy` pred)
