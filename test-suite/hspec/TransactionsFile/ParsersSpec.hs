module TransactionsFile.ParsersSpec (spec) where
import FundsRoller
import FundsRoller.TransactionsFile.Parsers
import Test.Hspec
import Data.Either
import Text.Parsec
import Text.Heredoc

fundBlockHeaderInput :: String
fundBlockHeaderInput = 
  [str|nome - 4324323432,,,
      |País da Entidade Emitente: 90809898,,,
      |NIF da Entidade Emitente: 504655256,,,
      |,,,,Valores da Transação,,,,,,,,Valores por Quota,,,,,,,
      |Entradas,,,,Valores na moeda original,,,,,,,,Valores na moeda original,,,,,,Valores em Euros,
      |Data Mov.,,,Tipo de Operação,Quantidade Total,,Preço,Valor Total,,,,,Encargos,,Valor,Câmbio,,,Encargos,Valor
      |17-10-2014,,,Subscrição de U.P. s,2.94,,136.05,400,,,,,0,,400,1,,,0,400|]


spec :: Spec
spec = parallel $ do
  describe "nameAndISINParser" $ do
    it "should succeed for a good value" $ 
      parse nameAndISINParser "" "Pictet Europe Index - LU0130731713" 
        `shouldBe` Right ("Pictet Europe Index", "LU0130731713") 
    it "should fail for an incomplete value without separator" $ 
      parse nameAndISINParser "" "nome -" `shouldSatisfy` isLeft
    it "should fail for an incomplete value with separator" $ 
      parse nameAndISINParser "" "nome - " `shouldSatisfy` isLeft

  describe "fundBlockHeader" $ do
    it "should succeed for a good value" $ 
      parse fundBlockHeader "" fundBlockHeaderInput
        `shouldBe` Right (Fund "nome" "4324323432" "90809898" "504655256") 
    it "should fail for an incomplete value without separator" $ 
      parse nameAndISINParser "" "nome -" `shouldSatisfy` isLeft
    it "should fail for an incomplete value with separator" $ 
      parse nameAndISINParser "" "nome - " `shouldSatisfy` isLeft
