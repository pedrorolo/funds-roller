module TransactionsFileSpec (spec) where
import BasicPrelude
import FundsRoller.Data
import FundsRoller.Expectations
import FundsRoller.Utils
import FundsRoller.TransactionsFile
import Text.Parsec as Parsec
import Data.Foldable

import Test.Hspec

spec :: Spec
spec = parallel $
  describe "the test csv" $ do
    let loadedFile = loadFile "resources/test.csv" :: IO (Either Parsec.ParseError [FundBlock])
    it "should be successfully read" $
      loadedFile >>= rightShouldSatisfy (const True)
    it "should contain 4 fund blocks" $
      length <$$> loadedFile >>= (`shouldBe` Right 4)
    it "should contain 18 transactions within the fundblocks" $ 
      (length . fold . fmap transactions) <$$> loadedFile >>= (`shouldBe` Right 18)
    -- describe "its first entry" $ do
    --   let firstEntry =  head <$$> loadedFile
    --   describe "fund" $ do
    --     let f = Transaction.fund <$$> firstEntry
    --     it "should have a non empty name" $
    --       f >>= rightShouldSatisfy (not . Text.null . Fund.name)
    --     it "should have a non empty nif" $
    --       f >>= rightShouldSatisfy (not . Text.null . Fund.nif)
    --     it "should have a non empty country" $
    --       f >>= rightShouldSatisfy (not . Text.null . Fund.country)